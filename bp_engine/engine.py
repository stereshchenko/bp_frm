import sys
from bp_engine.bp_creator.template_creator import TemplateCreator
from bp_engine.bp_creator.topology_creator import TopologyCreator
from bp_engine.configuration.config_creator import ConfigCreator
from bp_engine.configuration.fill_configuration import FillConfiguration
from bp_engine.file_manager.file_manager import FileManager


class Engine:
    def __init__(self, conf_id=None, cl_type=None, release_id=None, approval=None):
        self.release_id = release_id
        self.configuration_id = conf_id
        self.cluster_type = cl_type
        self.approval = approval
        self.store_path = FileManager.bp_store_path()

    def generate_bp(self):
        configurations = ConfigCreator.create_config(self.configuration_id, self.cluster_type, self.release_id,
                                                     self.approval)
        for configuration in configurations:
            fill_config = FillConfiguration.fill_config(self.release_id, self.approval, self.configuration_id)
            stack = str(fill_config[0].get('name'))
            path = "{0}/{1}/{2}/{3}".format(self.store_path, stack, configuration.get_id(),
                                                configuration.cluster_type)
            # create topology.json
            topology_creator = TopologyCreator(configuration)
            topology = topology_creator.create()
            # save in file
            file_path = "{0}/{1}".format(path, "topology.json")
            FileManager.save_as_file(path, "topology.json", topology)
            del topology
            #
            # create template
            template_creator = TemplateCreator(configuration)
            template = template_creator.create()
            # save in file
            file_path = "{0}/{1}".format(path, "template.json")
            FileManager.save_as_file(path, "template.json", template)
            del template


'''
# for testing
#  e = Engine(conf_id=None, cl_type=['test', 'Hdfs'], release_id="2810", approval="1")
# e.generate_bp()
'''


if __name__ == "__main__":
    # if you set some value as "", it will be converted to none
    for index, item in enumerate(sys.argv):
        if len(item) == 0:
            sys.argv[index] = None
        else:
            sys.argv[index] = str(sys.argv[index])
    for index, item in enumerate(sys.argv):
        print ("Index:%s / converted parameter:%s" % (index, item))
    # example value:"1069,1068" or "" => id "" will be None
    configuration_id = sys.argv[1]
    if configuration_id is not None:
        configuration_id = list(sys.argv[1].split(','))
        # TODO: remove after implementation list for configuration_id
        configuration_id = configuration_id[0]
    # example value:"test,Hdfs"
    cluster_type = sys.argv[2]
    if cluster_type is not None:
        cluster_type = list(sys.argv[2].split(','))
    # example value:"2810"
    release_id = sys.argv[3]
    # example value:"1"
    approval = sys.argv[4]
    e = Engine(conf_id=configuration_id, cl_type=cluster_type, release_id=release_id, approval=approval)
    e.generate_bp()

