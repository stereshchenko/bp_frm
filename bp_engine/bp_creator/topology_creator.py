from bp_engine.configuration.configuration import HostGroupType as GroupType
from bp_engine.entity.component import ComponentType
from bp_engine.entity.component import ParameterValue as HA

from bp_engine.file_manager.file_manager import *

from creator import Creator


class TopologyCreator(Creator):
    def __init__(self, configuration):
        super(TopologyCreator, self).__init__(configuration)

    def expected_dirs(self):
        """ Return expected files regarding to input configs """
        dirs = super(TopologyCreator, self).all_exp_dirs()
        # remove template folders
        if self.template in dirs:
            dirs.remove(self.template)
        if self.template_common in dirs:
            dirs.remove(self.template_common)
        return dirs

    def create(self):
        """ Create topology json, get json files from $service/topology; topology.json folders """
        print ("Configuration core path:%s" % self.path)
        for service in self.conf.services:
            print ("Services:%s" % service.name)
        merged_json_dict = {}
        # merge all files from services template folders ($service/template)
        exp_dirs = self.expected_dirs()
        path = self.path
        merged_json_dict = self.merge_filtered_json_files(path, exp_dirs, merged_json_dict)
        print merged_json_dict
        host_group = HostGroupsCreator(self.conf)
        merged_json_dict = self.insert_expected_values(self.merge_json(merged_json_dict, host_group.create()))
        json_str = json.dumps(merged_json_dict)
        return json_str


class HostGroupsCreator(Creator):
    def __init__(self, configuration):
        super(HostGroupsCreator, self).__init__(configuration)

    def create(self):
        """ Create template host group part of json. Return dict.
        {'host_groups': [{'hosts': [], 'name': 'gateway'},{'hosts': [], 'name': 'host_group_db'}]} """

        services = self.conf.services

        host_groups = self.conf.get_host_groups()
        components_dict = {key: [] for key in host_groups}
        for service in services:
            for component in service.components:
                print component
                groups = []
                comp_type = component.type
                comp_name = component.name
                if comp_type[0] == ComponentType.REQUIRED:
                    for group_type in host_groups:
                        groups.append(group_type)
                else:
                    for i, val in enumerate(comp_type):
                        if comp_type[i] == ComponentType.CLIENT or comp_type[i] == ComponentType.GATEWAY:
                            groups.append(GroupType.GATEWAY)
                        elif comp_type[i] == ComponentType.MASTER:
                            groups.append(GroupType.MASTERS)
                        elif comp_type[i] == ComponentType.SLAVE:
                            groups.append(GroupType.SLAVES)
                        # host group db contains clients only (for passing bp validation)
                        elif comp_type[i] == ComponentType.CLIENT:
                            groups.append(GroupType.ADD_HOST)
                    if component.ha_master == HA.YES:
                        groups.append(GroupType.HA)
                        if (GroupType.HA in host_groups) & comp_name.__eq__("ZOOKEEPER_SERVER"):
                            groups.append(GroupType.GATEWAY)

                for group in groups:
                    temp_list = components_dict.get(group, [])
                    temp_list.append(component)
                    if group in host_groups:
                        components_dict[group] = temp_list
        # {'host_groups': [{'cardinality': '1', 'name': 'host_group_master', 'components': [{'name': 'NAMENODE'}]}]}
        host_groups_dict = {"host_groups": [{"name": key, "cardinality": "1",
                                             "components": [({"name": value.name, "provision_action": value.provision_action})
                                                            if value.provision_action else ({"name": value.name}) for value in components_dict[key]]}
                                            for key in components_dict.keys()]}
        return host_groups_dict