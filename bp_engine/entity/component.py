from enum import Enum


class Component:
    def __init__(self, name, param):
        self.name = name
        self.ha = param.get("is_ha_only")
        self.kerberos = param.get("kerberos")
        self.type = str(param.get("type")).split(",")
        self.ha_master = param.get("ha_master", "no")
        self.provision_action = param.get("provision_action", "")

    def __str__(self):
        return '\ncomponent_name: ' + str(self.name) + '\nha: ' + self.ha + '\nkerberos: ' + \
               str(self.kerberos) + '\ntype: ' + str(self.type) + '\nha_master: ' + str(self.ha_master) + \
                '\nprovision_action: ' + str(self.provision_action)

    def __eq__(self, other):
        if isinstance(other, Component):
            return other.name == self.name
        return False

    def __ne__(self, other):
        return not self.__eq__(other)


class ParameterValue(object):
    # values which self.ha and self.kerberos may have
    BOTH = "both"
    YES = "yes"
    NO = "no"


class ComponentType(object):
    # values which self.type may have
    REQUIRED = "required"
    GATEWAY = "gateway"
    MASTER = "master"
    SLAVE = "slave"
    CLIENT = "client"

    values = [REQUIRED, GATEWAY, MASTER, SLAVE, CLIENT]
