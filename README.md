# bp_frm

1) For running bp_frm need to set PYTHONPATH on bp_frm folder
and install some modules
a) export PYTHONPATH='{local_path}/bp_frm'
b) pip install enum, simplejson
c) pip install requests

2) Run engine.py from bp_engine folder
Parameters: configuration_id, cluster_type, release_id, approval
Example: python engine.py "" "test,Hdfs" "2810" "1"
1069 - ha cluster
python engine.py "1069" "deployonly" "2810" "1"
3) Blueprints files will be saved in bp_store folder