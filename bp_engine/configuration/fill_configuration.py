from bp_engine.http_manager.http_manager import HttpManager
import json
from bp_engine.file_manager.file_manager import FileManager
from bp_engine.configuration.configuration import Configuration
from bp_engine.entity.component import Component
from bp_engine.entity.component import ParameterValue
from bp_engine.entity.service import Service


class FillConfiguration:
    def __init__(self):
        pass

    '''
    # return all components for service
    '''
    @staticmethod
    def get_component_for_service(service_name, security, ha, stack_type, stack):
        # path to component.json
        path_to_components_json = FileManager.bp_conf_store_path() + '/' + stack_type + '/' + stack +\
                                  '/cluster_components/' + service_name + '/component.json'
        print path_to_components_json
        components = []
        with open(path_to_components_json) as data_file:
            component_json = json.load(data_file)

        for component_key, component_value in component_json['components'].items():
            component = Component(component_key, component_value)
            components.append(component)
        '''
        # go though all component for service and check if component supported security and ha mode
        '''
        # if ha=true - remove all components where ha='no'
        if ha:
            components = filter(lambda comp: str(comp.ha) != ParameterValue.NO, components)
        # if ha=false - remove all components where ha='yes'
        if not ha:
            components = filter(lambda comp: str(comp.ha) != ParameterValue.YES, components)
        # if security = true - remove all components where security='no'
        if security:
            components = filter(lambda comp: str(comp.kerberos) != ParameterValue.NO, components)
        # if security = false - remove all components where security='yes'
        if not security:
            components = filter(lambda comp: str(comp.kerberos) != ParameterValue.YES, components)
        return components

    # return Service class
    @staticmethod
    def get_service(service_name, security, ha, stack_type, stack):
        return Service(service_name, FillConfiguration.get_component_for_service(service_name, security, ha,
                                                                                 stack_type, stack))

    # method return list of services for needed cluster_type
    @staticmethod
    def get_services_for_cluster_type(cluster_value, config_prop):
        install_dependencies = cluster_value['install_dependencies']
        # add overridden services to install_dependencies
        for key, value in config_prop.items():
            if str(key).startswith('install_') & str(value).__eq__('yes'):
                if key.split('_')[1] not in install_dependencies:
                    install_dependencies.append(key.split('_')[1])
                if key.split('_')[1] == 'atlas':
                    if 'ambari_infra' not in install_dependencies:
                        install_dependencies.append('ambari_infra')
                    if 'kafka' not in install_dependencies:
                        install_dependencies.append('kafka')
                    if 'hbase' not in install_dependencies:
                        install_dependencies.append('hbase')

        # add ranger/kerberos to install_dependencies according to dashboard response
        if str(config_prop['ranger']).__eq__('yes'):
            install_dependencies.append('ranger')
            if 'ambari_infra' not in install_dependencies:
                install_dependencies.append('ambari_infra')
            if str(config_prop['tde']) == 'yes':
                install_dependencies.append('kms')

        if config_prop['kerberos_server_type'] is not None:
            print 'kerberos was added in install dependencies'
            install_dependencies.append('kerberos')

        services = []
        for dependency in install_dependencies:
            service = FillConfiguration.get_service(dependency, config_prop['security'],
                                                    config_prop['is_ha_test'],
                                                    config_prop['stack_type'],
                                                    config_prop['stack'])
            services.append(service)
        # for testing, will remove
        '''
        services = []
        service = FillConfiguration.get_service("hdfs", security, ha)
        services.append(service)
        service = FillConfiguration.get_service("yarn", security, ha)
        services.append(service)
        '''
        #
        return services

    ''' 
    # return dependencies for cluster_type
    # stack - 2.6, 2.5, etc..
    # stack_type - hdp, ...
    # cluster_type - [..], if list empty - return all cluster_types in cluster.json
    '''
    @staticmethod
    def get_cluster_type_json(stack, stack_type, cluster_type):
        path_to_json = FileManager.bp_conf_store_path() + '/' + stack_type + '/' + stack + \
                       '/cluster_components/cluster.json'
        # get services list from cluster.json file
        with open(path_to_json) as data_file:
            cluster_json = json.load(data_file)

        print cluster_json
        if not cluster_type:
            return cluster_json
        else:
            for key, value in cluster_json.items():
                if key not in cluster_type:
                    cluster_json.pop(key)
            return cluster_json

    # fetch all required attributes from response and cluster.json
    # create configuration class
    @staticmethod
    def fill_config(release_id, is_approved, release_config_id):
        # if is_approval empty - use url with hardcoded is_approved
        # release_id should be not empty
        if not is_approved:
            is_approved = 1

        if not release_config_id:
            url = 'http://dashboard.qe.hortonworks.com:5000/hwqe-dashboard-api/v1/releaseconfigs' \
                  '?release_id={release_id}&is_approved={is_approved}'.format(release_id=release_id,
                                                                              is_approved=is_approved)
        else:
            url = 'http://dashboard.qe.hortonworks.com:5000/hwqe-dashboard-api/v1/releaseconfigs' \
                  '?id={config_id}'.format(config_id=release_config_id)
        print 'URL is: ' + url
        response = HttpManager.get_request(url)
        # dict with needed configs
        configs = FillConfiguration.parse_dashboard_response(response, 'release_configs')

        # Return configs from dashboard response
        return configs

    @staticmethod
    def parse_dashboard_response(response, param):
        configs = []
        for release_config in response[param]:
            # add here all needed property
            expected_config = {'ambari_db': None, 'kerberos_server_type': None, 'ranger': None,
                               'customized_services_users': None, 'security': None, 'name': None, 'stack': None,
                               'stack_type': None, 'is_ha_test': False, 'ha_services': None, 'tde': None,
                               'wire_encryption': False}
            for key, value_dict in release_config.items():
                if isinstance(value_dict, dict):
                    for key_internal, value in value_dict.items():
                        if key_internal in expected_config:
                            expected_config[key_internal] = value
                        # props with pattert install_ add to overridden services list
                        if str(key_internal).startswith('install_'):
                            expected_config[str(key_internal)] = value
            # parse stack, in json contains as example 'hdp-2.6.0.3'
            if expected_config['name']:
                expected_config['stack_type'] = str(expected_config['name']).split('-')[0]
                full_stack_name = str(expected_config['name']).split('-')[1]
                expected_config['stack'] = '{}.{}'.format(full_stack_name.split('.')[0], full_stack_name.split('.')[1])

            expected_config['id'] = release_config['id']

            if expected_config['ambari_db'] == 'maria':
                expected_config['ambari_db'] = 'mysql'

            if expected_config['wire_encryption'] == 'yes':
                expected_config['wire_encryption'] = True
            else:
                expected_config['wire_encryption'] = False

            if not expected_config['customized_services_users'] \
                    or expected_config['customized_services_users'].__eq__('false'):
                expected_config['customized_services_users'] = False
            else:
                expected_config['customized_services_users'] = True

            if not expected_config['is_ha_test'] \
                    or expected_config['is_ha_test'].__eq__('no'):
                expected_config['is_ha_test'] = False
            else:
                expected_config['is_ha_test'] = True


            if not expected_config['security'] \
                    or expected_config['security'].__eq__('no') \
                    or expected_config['security'].__eq__('n/a'):
                expected_config['security'] = False
            else:
                expected_config['security'] = True

            if expected_config['kerberos_server_type'].__eq__('n/a'):
                expected_config['kerberos_server_type'] = None

            # create configuration and add to list
            configs.append(expected_config)
        return configs
