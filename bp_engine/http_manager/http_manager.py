import json
import requests


class HttpManager():
    def __init__(self):
        pass

    # sent get request on giver url
    # return json response as dict
    @staticmethod
    def get_request(url):
        response = requests.get(url)
        return json.loads(response.text)
