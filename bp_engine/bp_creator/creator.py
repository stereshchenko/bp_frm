
from bp_engine.file_manager.file_manager import *
import random
import string


class Creator(object):
    # input config keys "services" - list [hdfs,yarn],
    # "kerberos_type" - string (ad or mit),
    # "stack" - string =  2.6, "stack_type" - string = hdp,
    # "customized_services_users" - boolean, "ambari_db" - String (mysql ....)
    # input_keys = ("services", "kerberos_type", "stack", "stack_type", "customized_services_users", "ambari_db")
    topology = "topology"
    template = "template"
    topology_common = "topology_common"
    template_common = "template_common"
    configurations = "configurations"
    stack_definition = "stack_definition"
    random_value_point="${RANDOM_VALUE}"

    kerberos = "kerberos"
    cstm_user = "cstm_user"
    we = "wire_encryption"
    db = "db"
    ha = "ha"

    # input configuration.Configuration
    def __init__(self, configuration):
        self.conf = configuration
        self.path = "{0}/{1}/{2}".format(FileManager.bp_conf_store_path(), self.conf.stack_type, self.conf.stack)
        # append list for keys which have same fields and must be append instead rewriting
        # self.append_list = ["services", "identities", "components"]

    def all_exp_dirs(self):
        """ Return expected files regarding to input configs """
        dirs = [self.topology, self.topology_common, self.template, self.template_common, self.db, self.stack_definition, self.configurations]
        for service in self.conf.get_services():
            dirs.append(service.name)
        if self.conf.kerberos_type is not None:
            dirs.append(self.kerberos)
            dirs.append(self.conf.kerberos_type)
        if self.conf.customized_services_users:
            dirs.append(self.cstm_user)
        if self.conf.ambari_db is not None:
            dirs.append(self.conf.ambari_db)
        if self.conf.ha:
            dirs.append(self.ha)
        if self.conf.wire_encryption:
            dirs.append(self.we)
        return dirs

    # TODO : implement separate HA for services
    '''
    def expected_dirs(self, service):
        """ Return filtered expected files regarding to input configs + services specific parameters"""
        dirs = self.all_exp_dirs()
        if self.conf.ha and self.conf.ha_services and service_list not in self.conf.ha_services_list:
            dirs.remove(self.ha)
        return dirs
    '''
    def to_list(self, input_dict):
        """ convert input dict {key:value} in list ["key:value"] and the list """
        json_list = []
        for item in input_dict.items():
            json_list.append({item[0]: item[1]})
        return json_list

    def merge_filtered_json_files(self, root_path, exp_dirs_list, core_dict):
        core_dict = self.merge_json_files(root_path, core_dict)
        dirs = FileManager.list_dir(root_path, Type.DIRS)
        filtered_dirs = self.filter(exp_dirs_list, dirs)
        print ("Filtered folders :%s" % filtered_dirs)
        for folder in filtered_dirs:
            path = root_path + "/" + str(folder)
            core_dict = self.merge_filtered_json_files(path, exp_dirs_list, core_dict)
        return core_dict

    def merge_json_files(self, path, core_dict):
        print ("PATH:" + str(path))
        files = FileManager.list_dir(path, Type.FILES)
        for f in files:
            full_path = path + "/" + str(f)
            data = FileManager.load_json(full_path)
            core_dict = self.merge_json(core_dict, data)
        return core_dict

    def insert_expected_values(self, json_dict):
        """ Replace str with key ${RANDOM_VALUE} on random value , Return dict """
        for key in json_dict.keys():
            if type(json_dict[key]) is str:
                json_dict[key] = json_dict[key].replace(self.random_value_point, ("rand" + ''.join(
                    random.choice(string.ascii_lowercase + string.digits + string.ascii_uppercase) for _ in range(14))))
            elif type(json_dict[key]) is dict:
                self.insert_expected_values(json_dict[key])
            elif type(json_dict[key]) is list:
                for i, val in enumerate(json_dict[key]):
                    self.insert_expected_values(val)
        return json_dict

    # TODO: implement mettod for implemented random string values and change values in result json [$service/template/security.json]
    def merge_json(self, main_dict, merge_dict):
        """ merge merge_dict into main_dict and return main_dict """
        for key in merge_dict.keys():
            if key in main_dict.keys():
                # append specific parts of json as those have same keys in list but different values for each service
                # services: [{same_keys},{same_keys},{same_keys}]
                # example is 'services' in template
                if type(merge_dict[key]) is list and type(main_dict[key]) is list:
                    # filter_key is key which be used for merging
                    # also if filter_key key is present, it means same keys are inside json but for different services
                    # example: "services": [{"name": "ZEPPELIN","identities": []},{"name": "SPARK","identities": []}}
                    filter_key = "name"
                    if "\'{0}\'".format(filter_key) in str(main_dict[key]):
                        wrapper_dict = {}
                        for item in main_dict[key]:
                            wrapper_dict[item[filter_key]] = item
                        for item in merge_dict[key]:
                            if item[filter_key] in wrapper_dict.keys():
                                wrapper_dict[item[filter_key]] = self.merge_json(wrapper_dict[item[filter_key]], item)
                            else:
                                wrapper_dict[item[filter_key]] = item
                        result_list = []
                        for item in wrapper_dict.items():
                            print (item)
                            result_list.append(item[1])
                        main_dict[key] = result_list
                    # if list is inside, topology => example is {"configurations": [ ]}
                    else:
                        merged_dict = {}
                        # to avoiding config duplicates
                        # take elements from list( dictionary) and merge them to one dictionary
                        for element in main_dict[key]:
                            merged_dict = self.merge_json(merged_dict, element)
                        # to avoiding config duplicates
                        # take elements from input list for merging and merge them to global dictionary
                        for element in merge_dict[key]:
                            merged_dict = self.merge_json(merged_dict, element)
                        # transfer back to list
                        merged_list = self.to_list(merged_dict)
                        # change old value on new in main dictionary
                        main_dict[key] = merged_list
                # rewrite value for final key (will be string)
                elif type(merge_dict[key]) is str:
                    main_dict[key] = merge_dict[key]
                    # TODO: implement generation and adding random value in json values (principal => value)
                    # TODO: implement merge of string and dictionary
                    # TODO: implement append for strings with same key in dict
                # still contains key inside a block, try to find final level of keys
                elif type(merge_dict[key]) is dict:
                    main_dict[key] = self.merge_json(main_dict[key], merge_dict[key])
                else:
                    print ("UNSUPPORTED TYPE:%s" % type(merge_dict[key]))
                    raise Exception('Unsupported json format')
            else:
                # add to core_dict key:value from merge_dict
                main_dict[key] = merge_dict[key]
        return main_dict

    def filter(self, expected_list, no_filtered_list):
        """ remove unexpected values from no_filtered_list, return expected_list: values which are present
        in both:expected_list and no_filtered_list only"""
        filtered_list = list(set(expected_list) & set(no_filtered_list))
        filtered_list = self.sort_dirs_by_priority(filtered_list)
        return filtered_list

    # method return sorted list regarding to priorities dictionary, if key absents in dict it will have sort value = 1
    def sort_dirs_by_priority(self, exp_dirs_list):
        """ method return sorted list regarding to priorities dictionary """
        priorities = {self.kerberos: 40, self.cstm_user: 50, self.ha: 60, "ranger": 100}
        folders_dir = {}
        for folder in exp_dirs_list:
            if folder in priorities.keys():
                folders_dir[folder] = priorities.get(folder)
            else:
                # set sort value to 1
                folders_dir[folder] = 1
        sorted_list = sorted(folders_dir, key=folders_dir.get)
        return sorted_list

    def create(self):
        json_dict = {}
        exp_dirs = self.expected_dirs()
        path = "{0}/{1}".format(self.path, self.path)
        json_dict = self.merge_filtered_json_files(path, exp_dirs, json_dict)
        json_str = json.dumps(json_dict, iterable_as_array=True)
        return json_str

