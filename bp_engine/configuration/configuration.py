from enum import Enum


class Configuration:
    # input config keys
    # services - list of services for the Configuration
    # configuration_dict - dict with configs for the Configuration :
    # - cluster_type : cluster_type name from dashboard response
    # - kerberos_type : mit/ad (str)
    # - stack : str = 2.6
    # - stack_type : str = HDP
    # - customized_services_users : bool
    # - is_ha_test : bool
    # - ambari_db : str (mysql/postgres..)
    def __init__(self, configurations_dict, services):
        # list of possible host groups
        self.host_groups = HostGroupType.values
        self.services = services
        self.cluster_type = configurations_dict.get("cluster_type", None)
        self.release_config_id = configurations_dict.get("id")
        self.kerberos_type = configurations_dict.get("kerberos_server_type", None)
        self.stack = configurations_dict.get("stack")
        self.stack_type = configurations_dict.get("stack_type").lower()
        self.customized_services_users = configurations_dict.get("customized_services_users", False)
        self.ambari_db = configurations_dict.get("ambari_db").lower()
        self.ha = configurations_dict.get("is_ha_test", False)
        self.ha_services_list = configurations_dict.get("ha_services", None)
        self.wire_encryption = configurations_dict.get("wire_encryption", False)

    def __str__(self):
        return 'Cluster_type is: ' + self.cluster_type
    # def __str__(self):
    #     return '\nservices: ' + ", ".join(self.services) + '\nkerberos_type: ' + self.kerberos_type + '\nstack: ' + \
    #            str(self.stack) + '\nstack_type: ' + str(self.stack_type) + '\ncustomized_services_users: ' + \
    #            str(self.customized_services_users) + '\nambari_db: ' + str(self.ambari_db)

    def get_services(self):
        """ Return list of services"""
        return self.services

    def get_host_groups(self):
        """ Return filtered host groups """
        filtered_host_groups = self.host_groups
        # add_host only in topology is needed
        if HostGroupType.ADD_HOST in filtered_host_groups:
            filtered_host_groups.remove(HostGroupType.ADD_HOST)
        # self.ha = True
        if not self.ha and HostGroupType.HA in filtered_host_groups:
            filtered_host_groups.remove(HostGroupType.HA)
        return filtered_host_groups

    def get_id(self):
        """ Return release_config_id"""
        return self.release_config_id


class HostGroupType(object):
    GATEWAY = "host_group_gateway"
    MASTERS = "host_group_master"
    SLAVES = "host_group_slave"
    HA = "host_group_ha_master"
    ADD_HOST = "add_host_group"

    values = [GATEWAY, MASTERS, SLAVES, HA, ADD_HOST]


class KerberosServerType(object):
    MIT = "mit"
    AD = "ad"

    values = [MIT, AD]
