from bp_engine.configuration.fill_configuration import FillConfiguration
from bp_engine.http_manager.http_manager import HttpManager
import json
from bp_engine.file_manager.file_manager import FileManager
from bp_engine.configuration.configuration import Configuration
from bp_engine.entity.component import Component
from bp_engine.entity.service import Service


class ConfigCreator:
    def __init__(self):
        pass

    '''
    # Method return list of configs 
    # release_id - required (str)
    # cluster_type - list, if list empty - use all cluster_type in cluster.json
    # approval - str, if empty - is_approved=1
    '''
    @staticmethod
    def create_config(release_config_id=None, cluster_type=None, release_id=None, approval=None):

        if not release_config_id and not release_id:
            raise ValueError('release_config_id or release_id required. At least one param should be present')

        config_properties = FillConfiguration.fill_config(release_id, approval, release_config_id)
        print config_properties

        configs = []
        for config_prop in config_properties:
            # for testing
            # move to loop below
            cluster_json = FillConfiguration.get_cluster_type_json(config_prop['stack'],
                                                                   config_prop['stack_type'],
                                                                   cluster_type)
            for key, value in cluster_json.items():
                cluster_type_config_prop = config_prop
                if 'overridden_kerberos_type' in value.keys():
                    cluster_type_config_prop['kerberos_server_type'] = value['overridden_kerberos_type']

                services = FillConfiguration.get_services_for_cluster_type(value,
                                                                           cluster_type_config_prop)

                conf_dict = {"cluster_type": key}
                conf_dict.update(cluster_type_config_prop)
                configs.append(Configuration(conf_dict, services))
                print '=========================='
                print conf_dict

        print configs.__len__()
        return configs
