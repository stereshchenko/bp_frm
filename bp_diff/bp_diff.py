import json
import requests
from jsondiff import diff

from bp_engine.file_manager.file_manager import FileManager


class BPDiff:
    def __init__(self):
        pass

    # Method check host groups count and each host group on component diff
    @staticmethod
    def host_group_diff(generated_bp, tested_bp):
        print "Finding BP diff in host groups..."
        generated_config = generated_bp['host_groups']
        tested_config = tested_bp['host_groups']
        print generated_config.__len__()
        print tested_config.__len__()
        print "Checking host groups"
        for host_group in generated_config:
            host_group_name = host_group['name']
            host_group_components = host_group['components']
            is_host_group_present = False
            for tested_host_group in tested_config:
                if tested_host_group['name'] == host_group_name:
                    is_host_group_present = True
                    tested_group_components = tested_host_group['components']
                    print tested_host_group['name'] + " === " + host_group_name
                    BPDiff.diff_in_host_group(host_group_components, tested_group_components)
            if not is_host_group_present:
                print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                print "HOST GROUP " + host_group_name + " NOT IN BP"
                print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

    # Method check configs from two BP's on equality
    @staticmethod
    def bp_diff(generated_bp, tested_bp):
        is_config_identity = True
        print "Finding BP diff..."
        generated_config = generated_bp['configurations']
        tested_config = tested_bp['configurations']
        print "Generated configs"
        print generated_config
        print "Tested configs"
        print tested_config
        for config in tested_config:
            for key, value in config.items():
                print "Configuration = " + key
                is_config_present = False
                for gen_config in generated_config:
                    for gen_key, gen_value in gen_config.items():
                        if gen_key == key:
                            is_config_present = True
                            diff_between_bp = diff(value, gen_value)
                            if diff_between_bp:
                                is_config_identity = False
                            print diff(value, gen_value)
                if not is_config_present:
                    print "ALL CONFIGS SHOULD BE IN BOTH BLUEPRINTS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                    is_config_identity = False
        if not is_config_identity:
            print "SOME CONFIGS NOT EQUALS!!!!!!!!!"

    @staticmethod
    def diff_in_host_group(host_group, tested_host_group):
        filtered_group = []
        for component in host_group:
            if component not in tested_host_group:
                filtered_group.append(component)
        print "Components which contains in one BP but not contains in another one"
        print filtered_group


path_to_bp_store = FileManager.bp_store_path() + '/hdp/2.6/1064/deployonly/topology.json'
# path_to_worked_bp_store = FileManager.test_bp_store_path() + '/1064/deployonly/topology.json'
path_to_worked_bp_store = FileManager.test_bp_store_path() + '/hdp-2.6.0.3/1064/worked/deployonly/topology.json'

with open(path_to_bp_store) as data_file:
    gen_bp = json.load(data_file)
with open(path_to_worked_bp_store) as data_file:
    test_bp = json.load(data_file)

# BPDiff.bp_diff(test_bp, gen_bp)
# BPDiff.bp_diff(gen_bp, test_bp)

BPDiff.host_group_diff(test_bp, gen_bp)
# BPDiff.host_group_diff(gen_bp, test_bp)

