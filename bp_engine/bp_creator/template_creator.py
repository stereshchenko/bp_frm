from bp_engine.file_manager.file_manager import *

from creator import Creator


class TemplateCreator(Creator):
    def __init__(self, configuration):
        super(TemplateCreator, self).__init__(configuration)

    def expected_dirs(self, service_list):
        """ Return expected files regarding to input configs """
        dirs = super(TemplateCreator, self).all_exp_dirs()
        # remove topology.json folders
        if self.topology in dirs:
            dirs.remove(self.topology)
        if self.topology_common in dirs:
            dirs.remove(self.topology_common)
        return dirs

    def create(self):
        """ Create template json, get json files from $service/template; template folders. Return str"""
        print ("Configuration path:%s" % self.path)
        print ("Services:%s" % self.conf.services)
        merged_json_dict = {}
        # merge all files from services template folders ($service/template)
        exp_dirs = self.expected_dirs(self.conf.services)
        path = self.path
        merged_json_dict = self.merge_filtered_json_files(path, exp_dirs, merged_json_dict)
        host_group = HostGroupsCreator(self.conf)
        merged_json_dict = self.insert_expected_values(self.merge_json(merged_json_dict, host_group.create()))
        # wrap merged configurations
        json_str = json.dumps(merged_json_dict)
        return json_str


class HostGroupsCreator(Creator):
    def __init__(self, configuration):
        super(HostGroupsCreator, self).__init__(configuration)

    def create(self):
        """ Create template host group part of json. Return dict.
        {'host_groups': [{'hosts': [], 'name': 'gateway'},{'hosts': [], 'name': 'host_group_db'}]} """
        host_groups = self.conf.get_host_groups()
        host_groups_dict = {"host_groups": [{"name": value, "hosts": []} for value in host_groups]}
        return host_groups_dict

