import os
import bp_engine as engine
from enum import Enum
import simplejson as json


class Type(object):
    FILES = "files"
    DIRS = "directories"
    PATH = "path"

    values = [FILES, DIRS, PATH]


class FileManager:
    def __init__(self):
        pass

    @staticmethod
    def bp_conf_store_path():
        bp_conf_store_folder = "bp_conf_store"
        path = "{0}/../{1}".format(str(os.path.dirname(engine.__file__)), bp_conf_store_folder)
        return path

    @staticmethod
    def bp_store_path():
        bp_store_folder = "bp_store"
        path = "{0}/../{1}".format(str(os.path.dirname(engine.__file__)), bp_store_folder)
        return path

    @staticmethod
    def test_bp_store_path():
        bp_store_folder = "worked_bp"
        path = "{0}/../{1}".format(str(os.path.dirname(engine.__file__)), bp_store_folder)
        return path

    @staticmethod
    def list_dir(path, element_type):
        if element_type not in Type.values:
            raise TypeError("Incorrect input: element_type: " + element_type)
        dirs = {}
        paths, dir_names, file_names = [], [], []

        for (path, dir, file) in os.walk(path):
            paths.extend(path)
            dir_names.extend(dir)
            file_names.extend(file)
            break

        dirs[Type.FILES] = file_names
        dirs[Type.DIRS] = dir_names
        dirs[Type.PATH] = paths
        return dirs.get(element_type)

    @staticmethod
    def load_json(path):
        with open(path) as data_file:
            data = json.load(data_file)
            data_file.close()
        return data

    @staticmethod
    def save_as_file(path, file_name, data):
        if not os.path.exists(path):
            os.makedirs(path)
        path_to_file = path + "/" + file_name
        f = open(path_to_file, 'wb')
        f.write(data)
        f.close()
