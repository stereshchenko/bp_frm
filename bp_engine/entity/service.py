class Service:
    def __init__(self, name, components):
        self.name = name
        self.components = components

    def get_components(self):
        """ Return list of components for service"""
        return self.components

    def __str__(self):
        for c in self.components:
            print c
        return '\nservice_name: ' + str(self.name)

    def __eq__(self, other):
        if isinstance(other, Service):
            return other.name == self.name
        return False

    def __ne__(self, other):
        return not self.__eq__(other)
